import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import * as Pages from './pages';
import { Provider } from 'react-redux';
import Preloader from './pages/Preloader';
import store from './redux/store';
import {
    BrowserRouter as Router,
    Route,
    Switch,
} from 'react-router-dom';
import ScrollToTop from './components/primary/ScrollToTop';

ReactDOM.render (
    <Provider store={store}>
        <Suspense fallback={<Preloader />}>
            <Router>
                <ScrollToTop />
                <Switch>
                    <Route exact path='/' component={Pages.Home} />
                    <Route exact path='/account' component={Pages.Account} />
                    <Route exact path='/travel' component={Pages.Travel} />
                    <Route exact path='/art' component={Pages.Art} />
                    <Route exact path='/design' component={Pages.Design} />
                    <Route exact path='/style' component={Pages.Style} />
                    <Route exact path='/guides' component={Pages.Guides} />
                    <Route exact path='/film' component={Pages.Film} />
                    <Route exact path='/playlist' component={Pages.Playlist} />
                    <Route exact path='/shop' component={Pages.Shop} />
                </Switch>
            </Router>
        </Suspense>
    </Provider>,
    document.getElementById('root')
);


//lime29
//cocacola!?
//ilovereact