import React, { Fragment, ReactElement } from 'react';
import Menu from '../components/primary/Menu';
import Content from '../components/secondary/Content';
import ContentData from '../json/pages/style.json';
import Footer from '../components/primary/Footer';
import FooterMenu from '../components/primary/FooterMenu';
import { useTranslation } from 'react-i18next';
import { I18Namespace } from '../i18n';


const Style: React.FC = (): ReactElement => {
    const [t] = useTranslation(I18Namespace.Main);
    return (
        <Fragment>
            <Menu page={t('Components.Primary.Menu.Search.PageTitle.style')}/>
            <Content contentData={ContentData} />
            <Footer />
            <FooterMenu />
        </Fragment>
    );
}

export default Style;