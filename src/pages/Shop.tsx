import React, { Fragment, ReactElement } from 'react';
import Menu from '../components/primary/Menu';
import Footer from '../components/primary/Footer';
import FooterMenu from '../components/primary/FooterMenu';
import { useTranslation } from 'react-i18next';
import { I18Namespace } from '../i18n';
import styles from '../less/pages/shop.less';
import ShopData from '../json/pages/shop.json';

const Shop: React.FC = (): ReactElement => {
    const [t] = useTranslation(I18Namespace.Main);

    function returnReactElement(): Array<ReactElement> {
        let returnMarkup: Array<ReactElement> = [];
        let key: number = 0;
        for (const item of Object.values(ShopData)) {
            if (item && Object.keys(item).length === 4) {
                returnMarkup.push(
                    <li key={key++} className={styles.product_list_item}>
                        <a className={styles.product_link}>
                            <img className={styles.product_image}
                                src={`./images/pages/shop/${item.image_info[0]}`}
                                alt={`${item.image_info[1]}`}
                            />
                            <h3 className={styles.product_title}>{t(item.title)}</h3>
                            <span className={styles.product_year}>{t(item.production_year)}</span>
                            <span className={styles.product_availability}>{t(item.availability)}</span>
                        </a>
                    </li>
                );
            }
        }
        return returnMarkup;
    }

    return (
        <Fragment>
            <Menu page={t('Components.Primary.Menu.Search.PageTitle.shop')}/>
            <div className={styles.shop_container}>
                <div className={styles.warning}>
                    <div className={styles.warning_content}>
                        <h3>{t('Components.Secondary.Shop.warning')}</h3>
                    </div>
                </div>
                <div className={styles.shop_content}>
                    <div className={styles.header}>
                        <div className={styles.last_volume}>
                            <div className={styles.header_image}>
                                <img src='./images/pages/shop/volume21.jpg' alt='Volume 21'/>
                            </div>
                            <div className={styles.header_description}>
                                <h2>{t('Components.Secondary.Shop.Header.Main.title')}</h2>
                                <p className={styles.date_and_price}>{t('Components.Secondary.Shop.Header.Main.date')}</p>
                                <p className={styles.date_and_price}>{t('Components.Secondary.Shop.Header.Main.price')}</p>
                                <p className={styles.remain}>{t('Components.Secondary.Shop.Header.Main.desc')}</p>
                                <p className={styles.remain}>{t('Components.Secondary.Shop.Header.Main.content')}</p>
                                <a className={styles.link}>{t('Components.Secondary.Shop.Header.Main.link')}</a>
                            </div>
                        </div>
                        <div className={styles.header_content}>
                            <div>
                                <h1>{t('Components.Secondary.Shop.Header.Addition.title')}</h1>
                                <h3>{t('Components.Secondary.Shop.Header.Addition.desc')}</h3>
                            </div>
                        </div>
                    </div>
                    <div className={styles.products_items}>
                        {returnReactElement()}
                    </div>
                </div>
            </div>
            <Footer />
            <FooterMenu />
        </Fragment>
    );
}

export default Shop;