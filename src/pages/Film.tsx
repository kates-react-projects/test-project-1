import React, { Fragment, ReactElement, useState } from 'react';
import Menu from '../components/primary/Menu';
import Footer from '../components/primary/Footer';
import FooterMenu from '../components/primary/FooterMenu';
import { BsPlayCircle, BsPlayCircleFill } from 'react-icons/bs';
import { useTranslation } from 'react-i18next';
import { I18Namespace } from '../i18n';
import FilmData from '../json/pages/film.json';
import styles from '../less/pages/film.less';

const Film: React.FC = (): ReactElement => {
    const hidden = 'hidden';
    const visible = 'visible';
    const min = '0%';
    const max = '100%';
    const [t] = useTranslation(I18Namespace.Main);
    const [hover, setHover] = useState<boolean>(false);
    const [visibility, setVisibility] = useState<DocumentVisibilityState>(visible);
    const [opacity, setOpacity] = useState<string>(max);

    function buttonClicked() {
        setVisibility(hidden);
        setOpacity(min);
    }

    function renderIcons(): ReactElement {
        return hover !== false
            ? <BsPlayCircleFill className={styles.icons} onMouseEnter={() =>  setHover(!hover)} onMouseLeave={() =>  setHover(!hover)}/>
            : <BsPlayCircle className={styles.icons} onMouseEnter={() =>  setHover(!hover)} onMouseLeave={() =>  setHover(!hover)}/>
    }

    function renderList(): Array<ReactElement> {
        const returnMarkup: Array<ReactElement> = [];
        let key = 0;

        for (const item of Object.values(FilmData.Additional)) {
            if (item.length == 2) {
                returnMarkup.push(
                    <li key={key++}>
                        <h4>{t(item[0])}</h4>
                        <p>{t(item[1])}</p>
                    </li>
                );
            } else if (item.length == 3) {
                returnMarkup.push(
                    <li key={key++}>
                        <h4>{t(item[0])}</h4>
                        <p>{t(item[1])}</p>
                        <p>{t(item[2])}</p>
                    </li>
                );
            }
        }
        return returnMarkup;
    }

    return (
        <Fragment>
            <Menu page={t('Components.Primary.Menu.Search.PageTitle.film')}/>
            <div className={styles.film_container}>
                <div className={styles.film_content}>
                    <div className={styles.suminagashi}>
                        <div
                            className={styles.icons_container}
                            onClick={() => buttonClicked()}
                            style={{
                                visibility: visibility,
                                opacity: opacity,
                                transition: 'visibility 1s linear, opacity 0.7s linear'
                            }}
                        >{renderIcons()}
                        </div>
                        <div className={styles.video_container}>
                            <iframe
                                className={styles.video}
                                src="https://player.vimeo.com/video/241943528?api=1&player_id=18816&color=ffffff&byline=0&badge=0&portrait=0&title=0&autopause=1"
                                frameBorder="0"
                            ></iframe>
                        </div>
                        <div className={styles.data_container}>
                            <div className={styles.main}>
                                <h2>{t(FilmData.Main.title)}</h2>
                                <p className={styles.date}>{t(FilmData.Main.date)}</p>
                                <p className={styles.description}>{t(FilmData.Main.desc)}</p>
                            </div>
                            <ul className={styles.additional}>
                                {renderList()}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
            <FooterMenu />
        </Fragment>
    );
}

export default Film;