import { lazy } from 'react';

export const Home = lazy( () => import('./Home'));
// export const Home = lazy(async () => {
//     await new Promise(resolve => setTimeout(resolve, 1000));
//     return import('./Home');
//   });
export const Account = lazy( () => import('./Account'));
export const Travel = lazy( () => import('./Travel'));
export const Art = lazy( () => import('./Art'));
export const Design = lazy( () => import('./Design'));
export const Style = lazy( () => import('./Style'));
export const Guides = lazy( () => import('./Guides'));
export const Film = lazy( () => import('./Film'));
export const Playlist = lazy( () => import('./Playlist'));
export const Shop = lazy( () => import('./Shop'));
