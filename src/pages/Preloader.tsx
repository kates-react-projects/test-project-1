import React, { ReactElement } from 'react';
import styles from '../less/pages/preloader.less';

const Preloader: React.FC = (): ReactElement => {
    return(
        <div className={styles.container}>
            <div className={styles.content}>
                <img className={styles.image} src='../Preloader/preloader.svg' alt='Loading'/>
                <h1 className={styles.title}>Loading...</h1>
            </div>
        </div>
    );
}

export default Preloader;
