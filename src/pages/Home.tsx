import React, { Fragment, ReactElement } from 'react';
import Menu from '../components/primary/Menu';
import Header from '../components/secondary/Header';
import Content from '../components/secondary/Content';
import ContentData from '../json/pages/home.json';
import Products from '../components/secondary/Products';
import Footer from '../components/primary/Footer';
import FooterMenu from '../components/primary/FooterMenu';

const Home: React.FC = (): ReactElement => {
    return (
        <Fragment>
            <Menu />
            <Header />
            <Content contentData={ContentData} />
            <Products />
            <Footer />
            <FooterMenu />
        </Fragment>
    );
}

export default Home;
