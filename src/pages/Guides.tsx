import React, { Fragment, ReactElement } from 'react';
import Menu from '../components/primary/Menu';
import FeaturesData from '../json/pages/guides.json';
import Footer from '../components/primary/Footer';
import FooterMenu from '../components/primary/FooterMenu';
import { useTranslation } from 'react-i18next';
import { I18Namespace } from '../i18n';
import styles from '../less/pages/guides.less';
import { Link, useHistory } from 'react-router-dom';

const Guides: React.FC = (): ReactElement => {
    const [t] = useTranslation(I18Namespace.Main);

    function returnReactElement(): Array<ReactElement> {
        let returnMarkup: Array<ReactElement> = [];
        let key: number = 0;
        for (const item of Object.values(FeaturesData)) {
            if (item && Object.keys(item).length === 3) {
                returnMarkup.push(
                    <li key={key++} className={styles.feature_list_item}>
                        <a className={styles.feature_link}>
                            <img className={styles.feature_image}
                                src={`./images/pages/guides/${item.image_info[0]}`}
                                alt={`${item.image_info[1]}`}
                            />
                            <span className={styles.feature_category}>{t(item.category)}</span>
                            <h3 className={styles.feature_title}>{t(item.title)}</h3>
                        </a>
                    </li>
                );
            }
        }
        return returnMarkup;
    }

    return (
        <Fragment>
            <Menu page={t('Components.Primary.Menu.Search.PageTitle.guides')}/>
            <div className={styles.container}>
                <div className={styles.guides}>
                    <img className={styles.guides_image} src="./images/pages/guides/city-guides.jpg" alt="Cereal Archive" />
                    <div className={styles.guides_context}>
                        <div className={styles.paragraph_container}>
                            <p className={styles.paragraph}>{t("Components.Secondary.Guides.Content.Paragraph.part1")}</p>
                            <span className={styles.paragraph_decoration}>—</span>
                            <p className={styles.paragraph}>{t("Components.Secondary.Guides.Content.Paragraph.part2")}</p>
                        </div>
                        <div className={styles.button_container}>
                            <Link to="/account" className={styles.link_like_button}>{t("Components.Secondary.Guides.Content.button")}</Link>
                        </div>
                    </div>
                </div>
                <div className={styles.content}>
                    <p className={styles.section_feature_title}>Online Guides</p>
                </div>
                <div className={styles.feature_products}>
                    <p className={styles.section_feature_title}>Printed Guides</p>
                    <ul className={styles.feature_list}>
                        {returnReactElement()}
                    </ul>
                </div>
            </div>
            <Footer />
            <FooterMenu />
        </Fragment>
    );
}

export default Guides;