import React, { Fragment, ReactElement, useEffect, useState } from 'react';
import Menu from '../components/primary/Menu';
import Footer from '../components/primary/Footer';
import FooterMenu from '../components/primary/FooterMenu';
import { useTranslation } from 'react-i18next';
import { I18Namespace } from '../i18n';
import { connect } from 'react-redux';
import { ConnectedProps, useDispatch, useSelector } from 'react-redux';
import { accountData, login, register } from '../redux/logins';
import { selectedUserAccount, checkUserAccount, addUserAccount, hashing } from '../redux/user-events';
import { RootState } from '../redux/store';
import { useHistory } from 'react-router-dom';
import CryptoJS from 'crypto-js';
import styles from '../less/pages/account.less';

const emailRegex = new RegExp(/^(?:[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/);

const mapState = (state: RootState) => ({
    selectedLogin: selectedUserAccount(state)
});
const mapDispatch = {
    checkUserAccount
}
const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

interface Props extends PropsFromRedux {};

const Account: React.FC<Props> = ({selectedLogin, checkUserAccount}): ReactElement => {
    const [t] = useTranslation(I18Namespace.Main);
    const dispatch = useDispatch();
    const authorization = useSelector(accountData);
    const [errorMessage, setErrorMessage] = useState<string>('');
    const [throwError, setThrowError] = useState<boolean>(false);
    const history = useHistory();

    useEffect(() => {
        checkUserAccount();
    },[]);

    const handleClick = () => {
        document.onclick = (e) => {
            if ((e.target as Element).id === 'loginButton') {
                if (!authorization) {
                    console.log('LOG IN LOGIN');
                    if (loginAttempt()) {
                        dispatch(login());
                        setTimeout(() => {
                            history.push('/');
                        }, 1000)
                    }
                }
            } else if (((e.target as Element).id === 'registerButton')) {
                if (!authorization) {
                    console.log('LOG IN REGISTER');
                    if (registration()) {
                        dispatch(addUserAccount());
                        dispatch(register());
                        setTimeout(() => {
                            history.push('/');
                        }, 1000)
                    }
                }
            }
        }
    }

    function loginAttempt(): boolean {
        const email = (document.getElementById('loginEmail') as HTMLInputElement)?.value;
        const password = (document.getElementById('loginPassword') as HTMLInputElement)?.value;
        let checking = false;
        selectedLogin.filter((item) => {
            if (item.hash === hashing(email, password).toString(CryptoJS.enc.Hex)) {
                setThrowError(false);
                checking = true;
            }
        })
        if (!checking) {
            setErrorMessage('Your email or password is wrong! Try again.');
            setThrowError(true);
        }
        return checking;
    }

    function registration(): boolean {
        const name = (document.getElementById('registerUserName') as HTMLInputElement)?.value;
        const email = (document.getElementById('registerEmail') as HTMLInputElement)?.value;
        const password = (document.getElementById('registerPassword') as HTMLInputElement)?.value;
        let checking = true;
        selectedLogin.filter((item) => {
            if (item.email === email) {
                setErrorMessage('This email already exists!');
                setThrowError(true);
                checking = false;
            }
        })
        if ((name === '') || (email === '') || (password === '')) {
            setErrorMessage('Sorry, you must complete all fields!');
            setThrowError(true);
            checking = false;
        } else if (checking) {
            if (emailRegex.test(email)) {
                setThrowError(false);
            } else {
                setErrorMessage('Sorry, your input email is wrong! Try again.');
                setThrowError(true);
                checking = false;
            }
        }
        return checking;
    }

    function renderErrorMessage(): ReactElement | void{
        if (throwError && errorMessage !== '') {
            return (
                <div className={styles.account_error}>
                    <div>
                        <h3 className={styles.error_container}>ERROR:</h3>
                        <h3 className={styles.error_message}>{errorMessage}</h3>
                    </div>
                </div>
            );
        }
    }

    return (
        <Fragment>
            <Menu />
            <div className={styles.account_container}>
                <div className={styles.account_content}>
                    <h2 className={styles.content_title}>{t("Components.Secondary.Account.title")}</h2>
                    {renderErrorMessage()}
                    <span>
                        <div className={styles.login_container}>
                            <p className={styles.title}>{t("Components.Secondary.Account.Login.title")}</p>
                            <label className={styles.label_text}>
                                {t("Components.Secondary.Account.Login.input")}
                                <input className={styles.input} id="loginEmail" type='email'/>
                            </label>
                            <label className={styles.label_text}>
                                {t("Components.Secondary.Account.Login.password")}
                                <input className={styles.input} id="loginPassword" type='password'/>
                            </label>
                            <div>
                                <button onClick={handleClick} id='loginButton' className={styles.buttons}>{t("Components.Secondary.Account.Login.button")}</button>
                                <label>
                                    <input type='checkbox'/>
                                    {t("Components.Secondary.Account.Login.save-account")}
                                </label>
                            </div>
                            <a>{t("Components.Secondary.Account.Login.additional-option")}</a>
                        </div>
                        <div className={styles.register_container}>
                            <p className={styles.title}>{t("Components.Secondary.Account.Register.title")}</p>
                            <label className={styles.label_text}>
                                {t("Components.Secondary.Account.Register.name")}
                                <input className={styles.input} id="registerUserName" type='text'/>
                            </label>
                            <label className={styles.label_text}>
                                {t("Components.Secondary.Account.Register.input")}
                                <input className={styles.input} id="registerEmail" type='email'/>
                            </label>
                            <label className={styles.label_text}>
                                {t("Components.Secondary.Account.Register.password")}
                            <input className={styles.input} id="registerPassword" type='password'/>
                            </label>
                            <p>
                                {t("Components.Secondary.Account.Register.desc")}
                                <a className={styles.policy_link}>{t("Components.Secondary.Account.Register.reference")}</a>.
                            </p>
                            <button onClick={handleClick} id='registerButton' className={styles.buttons}>{t("Components.Secondary.Account.Register.button")}</button>
                        </div>
                    </span>
                </div>
            </div>
            <Footer />
            <FooterMenu />
        </Fragment>
    );
}

export default connector(Account);
