import i18n from 'i18next';
import Backend from 'i18next-http-backend';
import { initReactI18next } from 'react-i18next';

/**
 * Namespaces of i18next.
 */
export enum I18Namespace {
    Main = 'main',
}

/**
 * Language codes in ISO 639-1 format used in application.
 */
export enum LanguageCodes {
    English = 'en',
    Russian = 'ru',
}

i18n
    .use(Backend)
    .use(initReactI18next)
    .init({
        backend: {
            loadPath: '/locales/{{lng}}/{{ns}}.json'
        },
        lng: LanguageCodes.English,
        fallbackLng: LanguageCodes.English,
        debug: (process.env.NODE_ENV || 'development') === 'development',
        interpolation: {
            // Not needed for react as it escapes by default.
            escapeValue: false,
        },
        react: {
            useSuspense: false,
            wait: false,
        },
        defaultNS: I18Namespace.Main,
        ns: I18Namespace.Main,
        load: 'currentOnly',
    });

export default i18n;
