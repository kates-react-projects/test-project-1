import { Action } from "redux";
import { ThunkAction } from "redux-thunk";
import { RootState } from "./store";
import CryptoJS from 'crypto-js';
import 'regenerator-runtime/runtime';

interface UserAccount {
    id: number;
    name: string;
    email: string;
    hash: string;
}

interface UserAccountsState {
    userAccount: Record<UserAccount['id'], UserAccount>
    userAccountArray: UserAccount['id'][];
}

const initialState: UserAccountsState = {
    userAccount: {},
    userAccountArray: []
}

const LOAD_REQUEST = 'userEvents/load_request';
const LOAD_ACCOUNTS = 'userEvents/load_accounts';
const FAIL_LOADING = 'userEvents/fail_loading';
const CREATE_REQUEST = 'userEvents/create_request';
const ADDED_ACCOUNT = 'userEvents/added_account';
const FAIL_ADDING = 'userEvents/fail_adding';

interface LoadRequestAction extends Action<typeof LOAD_REQUEST> {};
interface LoadUserAccountsAction extends Action<typeof LOAD_ACCOUNTS> {
    payload: {
        accounts: UserAccount[];
    }
};
interface FailUserAccountLoading extends Action<typeof FAIL_LOADING>{
    error: string
};
interface CreateRequestAction extends Action<typeof CREATE_REQUEST> {};
interface AddedNewAccount extends Action<typeof ADDED_ACCOUNT> {
    payload: {
        account: UserAccount;
    }
};
interface FailUserAccountAdding extends Action<typeof FAIL_ADDING> {
    error: string
}

const selectedUserAccountState = (rootState: RootState) => rootState.userEvents;
export const selectedUserAccount = (rootState: RootState) => {
    const state = selectedUserAccountState(rootState);
    return state.userAccountArray.map(id => state.userAccount[id]);
}

export const checkUserAccount = (): ThunkAction<
    Promise<void>,
    RootState,
    undefined,
    LoadRequestAction | LoadUserAccountsAction | FailUserAccountLoading
    > => async (dispatch, getState) => {
    dispatch({
        type: LOAD_REQUEST
    })
    try {
        const response = await fetch('http://localhost:3001/account');
        const accounts: UserAccount[] = await response.json();

        dispatch({
            type: LOAD_ACCOUNTS,
            payload: {accounts: accounts}
        })

    } catch (e) {
        dispatch({
            type: FAIL_LOADING,
            error: 'Failed to load the data of logins.'
        })
    }
};

export const hashing = (email: string, password: string) => {
    let hash = CryptoJS.SHA256(email.concat(password));
    return hash;
}

export const addUserAccount = (): ThunkAction<
Promise<void>,
RootState,
undefined,
CreateRequestAction | AddedNewAccount | FailUserAccountAdding
> => async (dispatch) => {
    dispatch({
        type: CREATE_REQUEST
    })

    try {
        const account: Omit<UserAccount, 'id'> = {
            name: (document.getElementById('registerUserName') as HTMLInputElement)?.value,
            email: (document.getElementById('registerEmail') as HTMLInputElement)?.value,
            hash: hashing(
                (document.getElementById('registerEmail') as HTMLInputElement)?.value,
                (document.getElementById('registerPassword') as HTMLInputElement)?.value).toString(CryptoJS.enc.Hex)
        };

        const response = await fetch('http://localhost:3001/account', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(account)
        });

        const addedUserAccount: UserAccount = await response.json();

        dispatch({
            type: ADDED_ACCOUNT,
            payload: {account: addedUserAccount}
        })

    } catch (e) {
        dispatch({
            type: FAIL_ADDING,
            error: "Wrong adding user data into the json-server."
        })
    }
};

const userEventsReducer = (state: UserAccountsState = initialState, action: LoadUserAccountsAction | AddedNewAccount) => {
    switch (action.type) {
        case LOAD_ACCOUNTS:
            const {accounts} = action.payload;
            return {
                ...state,
                userAccountArray: accounts.map(({ id }) => id),
                userAccount: accounts.reduce<UserAccountsState['userAccount']>((userAccount, account) => {
                  userAccount[account.id] = account;
                  return userAccount;
                }, {})
              };
        case ADDED_ACCOUNT:
            const {account} = action.payload;
            return {
                ...state,
                userAccountArray: [...state.userAccountArray, account.id],
                userAccount: { ...state.userAccount, [account.id]: account }
            };
        default:
            return state;
    }
}

export default userEventsReducer;