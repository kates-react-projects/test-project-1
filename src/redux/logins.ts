import { Action } from "redux";
import { RootState } from "./store";

interface LoginState {
    authorization: boolean
}

const LOGIN = 'account/login';
const REGISTER = 'account/register';
const SIGNOUT = 'account/signout';

type LoginAction = Action<typeof LOGIN>;
type RegisterAction = Action<typeof REGISTER>;
type SignoutAction = Action<typeof SIGNOUT>;

export const login = (): LoginAction => ({
    type: LOGIN
})

export const register = (): RegisterAction => ({
    type: REGISTER
})

export const signout = (): SignoutAction => ({
    type: SIGNOUT
})

export const accountData = (rootState: RootState) => rootState.account.authorization;

const initialState: LoginState = {
    authorization: false
}

const loginReducer = (state: LoginState = initialState, action: LoginAction | RegisterAction | SignoutAction) => {
    switch(action.type) {
        case LOGIN:
            return {...state, authorization: true};

        case REGISTER:
            return {...state, authorization: true};

        case SIGNOUT:
            return {...state, authorization: false};

        default:
            return state;
    }
}

export default loginReducer;