import React, { ReactElement } from 'react';
import { I18Namespace } from '../../i18n';
import { AiOutlineInstagram, AiOutlineTwitter } from 'react-icons/ai';
import { GrFacebookOption } from 'react-icons/gr';
import { FaPinterestP } from 'react-icons/fa';
import { useTranslation } from 'react-i18next';
import styles from '../../less/components/footer.less';
import { Link, useHistory, useLocation } from 'react-router-dom';

const Footer: React.FC = (): ReactElement => {
    const footerTextContent: string = "Components.Primary.Footer.desc";
    const footerContent = {
        "Components.Primary.Footer.Content.Info.title": ["Components.Primary.Footer.Content.Info.1", "Components.Primary.Footer.Content.Info.2", "Components.Primary.Footer.Content.Info.3"],
        "Components.Primary.Footer.Content.Media.title": ["Components.Primary.Footer.Content.Media.1", "Components.Primary.Footer.Content.Media.2"],
        "Components.Primary.Footer.Content.Website.title": ["Components.Primary.Footer.Content.Website.1", "Components.Primary.Footer.Content.Website.2", "Components.Primary.Footer.Content.Website.3"],
    }
    const [t] = useTranslation(I18Namespace.Main);
    const location = useLocation();

    function homeClick() {
        if (location.pathname === '/') {
            window.scrollTo(0, 0);
        }
    }

    function renderReactElement(): Array<ReactElement> {
        let returnMarkup: Array<ReactElement> = [];
        let tempContainer: Array<ReactElement> = [];
        let key: number = 0;
        for (const item of Object.entries(footerContent)) {
            let index: number = 0;
            if (item && item[1].length > 0) {
                for (const element of Object.values(item[1])) {
                    tempContainer.push(
                        <li key={index++} className={styles.footer_list_item}><a >{t(element)}</a></li>
                    );
                }
                returnMarkup.push(
                    <ul key={key++} className="footer_list">
                        <p className={styles.footer_list_title}>{t(item[0])}</p>
                        {tempContainer}
                    </ul>
                );
            }
            index = 0;
            tempContainer = [];
        }
        return returnMarkup;
    }

    return (
        <div className={styles.footer_container}>
            <div className={styles.footer_content}>
                <div>
                    <p className={styles.footer_text}>{t(footerTextContent)}</p>
                    <div className={styles.footer_logo_container}>
                        <p className={styles.simbol_container}>©</p>
                        <Link onClick={() => homeClick()} className={styles.footer_logo_link} to='/'>
                            <img className={styles.footer_logo} src={'.//Logo/cereal.svg'} alt="Logo"/>
                        </Link>
                    </div>
                </div>
                <div className={styles.footer_list_container}>
                    {renderReactElement()}
                </div>
                <div className={styles.newsletter_container}>
                    <p className={styles.footer_list_title}>{t("Components.Primary.Footer.Email.desc")}</p>
                    <form className={styles.footer_form}>
                        <label>
                            <input id="inputEmail" type="e-mail" placeholder={t("Components.Primary.Footer.Email.placeholder")}/>
                        </label>
                        <input type="submit" value="→"/>
                    </form>
                    <div className={styles.icons_container}>
                        <a className={styles.footer_icon_links} href="https://www.instagram.com/cerealmag/"><AiOutlineInstagram /></a>
                        <a className={styles.footer_icon_links} href="https://twitter.com/cerealmag"><AiOutlineTwitter /></a>
                        <a className={styles.footer_icon_links} href="https://www.facebook.com/cerealmag"><GrFacebookOption /></a>
                        <a className={styles.footer_icon_links} href="https://www.pinterest.co.uk/cerealmag/"><FaPinterestP /></a>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer;
