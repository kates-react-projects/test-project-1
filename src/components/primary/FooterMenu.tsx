import React from 'react';
import { withTranslation, WithTranslation } from 'react-i18next';
import { I18Namespace } from '../../i18n';
import styles from '../../less/components/footer-menu.less';

interface FooterProps extends WithTranslation {}

interface FooterState {
    click: boolean,
    visibility: DocumentVisibilityState,
    opacity: number
}

class FooterMenu extends React.Component<FooterProps, FooterState> {
    readonly hidden = 'hidden';
    readonly visible = 'visible';
    readonly min = 0;
    readonly max = 100;

    constructor(props: Readonly<FooterProps>) {
        super(props);
        this.state = {
            click: false,
            visibility: this.visible,
            opacity: this.max
        }
        this.closePopUpWindow = this.closePopUpWindow.bind(this);
    }

    closePopUpWindow() {
        this.setState({
            click: !this.state.click,
            visibility: this.state.click !== false ? this.visible : this.hidden,
            opacity: this.state.click !== false ? this.max : this.min
        })
    }

    render() {
        return (
            <div className={styles.container}
                style={{
                    visibility: `${this.state.visibility}`,
                    opacity: `${this.state.opacity}`,
                    transition: 'visibility 0.3s linear, opacity 0.2s linear'
                }}
            >
                <div className={styles.content}>
                    <div className={styles.footerMenu_content}>
                        <p>{this.props.t("Components.Primary.FooterMenu.desc")}</p>
                        <div className={styles.email_container}>
                            <input type="email" placeholder={this.props.t("Components.Primary.FooterMenu.placeholder")}></input>
                            <button onClick={() => this.closePopUpWindow()} className={styles.button_okey}>ok</button>
                        </div>
                    </div>
                    <button onClick={() => this.closePopUpWindow()} className={styles.close_menu_button}>
                        {this.props.t("Components.Primary.FooterMenu.button")}
                    </button>
                </div>
            </div>
        );
    }
}

export default withTranslation(I18Namespace.Main)(FooterMenu);
