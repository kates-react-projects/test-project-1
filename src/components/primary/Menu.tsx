import React, { Dispatch, Fragment, ReactElement } from 'react';
import { GoSearch } from 'react-icons/go';
import { GrClose } from 'react-icons/gr';
import { VscMenu } from 'react-icons/vsc';
import { FiLogOut } from 'react-icons/fi';
import { Link } from "react-router-dom";
import { UseTranslationResponse, WithTranslation, withTranslation } from 'react-i18next';
import { I18Namespace, LanguageCodes } from '../../i18n';
import { connect, ConnectedProps } from 'react-redux';
import { checkUserAccount, selectedUserAccount } from '../../redux/user-events';
import { RootState } from '../../redux/store';
import styles from '../../less/components/menu.less';
import { accountData, signout } from '../../redux/logins';

const mapState = (state: RootState) => ({
    selectedLogin: selectedUserAccount(state),
    authorization: accountData(state)
});
const mapDispatch = (dispatch: Function) => {
    return {
        // signOut: () => dispatch({type: 'SIGNOUT'}),
        signOut: () => dispatch(signout()),

    }
}
const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

interface MenuProps extends PropsFromRedux, WithTranslation {
    page?: UseTranslationResponse<string>
};

interface MenuState {
    searchClick: boolean,
    searchVisibility: DocumentVisibilityState,
    searchOpacity: number,
    sidebarClick: boolean,
    sidebarVisibility: DocumentVisibilityState,
    sidebarOpacity: number,
    pageTitleVisibility: DocumentVisibilityState,
    pageTitleOpacity: number,
};

class Menu extends React.Component<MenuProps, MenuState> {

    readonly hidden = 'hidden';
    readonly visible = 'visible';
    readonly min = 0;
    readonly max = 100;
    readonly emailInputRef = React.createRef<HTMLInputElement>();
    readonly sidebarButton = document.getElementsByClassName('menu_button') as HTMLCollectionOf<HTMLElement>;
    readonly sidebarContainer = document.getElementsByClassName('sidebar') as HTMLCollectionOf<HTMLElement>;
    _isMounted: boolean;

    constructor(props: MenuProps) {
        super(props);
        this.state = {
            searchClick: false,
            searchVisibility: this.hidden,
            searchOpacity: this.min,
            sidebarClick: false,
            sidebarVisibility: this.hidden,
            sidebarOpacity: this.min,
            pageTitleVisibility: this.visible,
            pageTitleOpacity: this.max
        }
        this._isMounted = false;
        this.searchField = this.searchField.bind(this);
        this.pageTitle = this.pageTitle.bind(this);
        this.sidebar = this.sidebar.bind(this);
        this.signOutAction = this.signOutAction.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentDidUpdate() {
        document.onclick = (e) => {
            if ((e.target as Element).id !== 'menuButton' && this.state.sidebarClick) {
                if (this._isMounted) {
                    this.sidebar();
                }
            }
        }

        if (this.state.searchClick) {
            this.emailInputRef.current?.focus();
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    signOutAction() {
        if (this.props.authorization) {
            this.props.signOut();
            console.log("SIGN OUT");
        }
    }

    searchField() {
        this.setState({
            searchClick: !this.state.searchClick,
            searchVisibility: this.state.searchClick !== false ? this.hidden : this.visible,
            searchOpacity: this.state.searchClick !== false ? this.min : this.max
        })
    }

    pageTitle() {
        this.setState({
            searchClick: !this.state.searchClick,
            pageTitleVisibility: this.state.searchClick !== false ? this.visible : this.hidden,
            pageTitleOpacity: this.state.searchClick !== false ? this.max : this.min
        })
    }

    sidebar() {
        this.setState({
            sidebarClick: !(this.state.sidebarClick),
            sidebarVisibility: this.state.sidebarClick !== false ? this.hidden : this.visible,
            sidebarOpacity: this.state.sidebarClick !== false ? this.min : this.max
        })
    }

    renderAccountPage(): ReactElement {
        if (this.props.authorization) {
            return (
                <Link onClick={() => this.signOutAction()} className={styles.signout_link} to="/">
                    <FiLogOut className={styles.signout_icon} />
                </Link>
            );
        } else {
            return (
                <Link className={styles.account_link} to="/account">
                    <img className={styles.account_icon} src={'./icons/account.svg'} alt="Account"/>
                </Link>
            );
        }
    }

    renderSearchIcons(): ReactElement {
        return (this.state.searchVisibility !== this.visible
            ? <GoSearch className={styles.search_icon} />
            : <GrClose className={styles.search_icon}/>
        );
    }

    renderSidebarIcons(): ReactElement {
        return (this.state.sidebarVisibility !== this.visible
            ? <VscMenu className={styles.menu_icon} id='menuButton' onClick={() => this.sidebar()}/>
            : <GrClose  className={styles.menu_icon_special_style} id='menuButton' onClick={() => this.sidebar()}/>
        );
    }

    renderPageTitle(): ReactElement | void {
        if (this.props.page) {
            return (
                <h1 className={styles.page_title}
                    style={{
                        visibility: `${this.state.pageTitleVisibility}`,
                        opacity: `${this.state.pageTitleOpacity}`,
                        transition: `${this.state.pageTitleVisibility === this.visible ? 'visibility 0.15s linear, opacity 0.7s linear' : 'visibility 0s linear, opacity 0s linear'}`,
                    }}
                >
                    {this.props.page}
                </h1>
            );
        }
    }

    onLanguageChange() {
        if (this.props.i18n.language == LanguageCodes.English) {
            this.props.i18n.changeLanguage(LanguageCodes.Russian);
        } else {
            this.props.i18n.changeLanguage(LanguageCodes.English);
        }
    }

    render() {
        return (
            <div className={styles.menu_container}>
                <div className={styles.menu_content}>
                    <Link className={styles.logo_home_link} to="/">
                        <img className={styles.logo} src={'./logo/cereal.svg'} alt="Logo" />
                    </Link>
                    <button onClick={this.onLanguageChange.bind(this)} className={styles.change_language}>
                        {this.props.t('Components.Primary.Menu.button')}
                    </button>
                    <span className={styles.search_field_container}>
                        <input id='inputField' ref={this.emailInputRef}
                            style={{
                                opacity: `${this.state.searchOpacity}`,
                                transition: `${this.state.searchVisibility === this.visible ? 'visibility 0.15s linear, opacity 0.7s linear' : 'visibility 0s linear, opacity 0s linear'}`
                            }}
                            className={styles.search_input} type='text'
                            placeholder={this.props.t("Components.Primary.Menu.Search.placeholder")}
                        />
                        {this.renderPageTitle()}
                    </span>
                    {this.renderAccountPage()}
                    <button onClick={() => {this.searchField(), this.pageTitle()}} className={styles.search_button}>
                        {this.renderSearchIcons()}
                    </button>
                    <span className={styles.menu_link}>
                        <button className={styles.menu_button}>
                            {this.renderSidebarIcons()}
                        </button>
                        <ul className={styles.sidebar}
                            style={{
                                visibility: `${this.state.sidebarVisibility}`,
                                opacity: `${this.state.sidebarOpacity}`,
                                transition: 'visibility 0.3s linear, opacity 0.2s linear'
                            }}
                        >
                            <Link className={styles.sidebar_links} to='/travel'>
                                <li>{this.props.t("Components.Primary.Menu.Sidebar.List.travel")}</li>
                            </Link>
                            <Link className={styles.sidebar_links} to='/art'>
                                <li>{this.props.t("Components.Primary.Menu.Sidebar.List.art")}</li>
                            </Link>
                            <Link className={styles.sidebar_links} to='/design'>
                                <li>{this.props.t("Components.Primary.Menu.Sidebar.List.design")}</li>
                            </Link>
                            <Link className={styles.sidebar_links} to='/style'>
                                <li>{this.props.t("Components.Primary.Menu.Sidebar.List.style")}</li>
                            </Link>
                            <Link className={styles.sidebar_links} to='/guides'>
                                <li>{this.props.t("Components.Primary.Menu.Sidebar.List.guides")}</li>
                            </Link>
                            <Link className={styles.sidebar_links} to='/shop'>
                                <li>{this.props.t("Components.Primary.Menu.Sidebar.List.shop")}</li>
                            </Link>
                        </ul>
                    </span>
                </div>
            </div>
        );
    }
}

export default withTranslation(I18Namespace.Main)(connector(Menu));
