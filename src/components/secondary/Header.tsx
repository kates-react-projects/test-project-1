import React, { Fragment, ReactElement } from 'react';
import { Link } from 'react-router-dom';
import { WithTranslation, withTranslation } from "react-i18next";
import { I18Namespace } from '../../i18n';
import CategoryData from '../../json/components/header.json';
import styles from '../../less/components/header.less';

interface HeaderProps extends WithTranslation {}

interface HeaderState {
    category: string,
    background: string,
    hover: boolean
}

class Header extends React.Component<HeaderProps, HeaderState> {

    constructor(props: HeaderProps | Readonly<HeaderProps>) {
        super(props);
        this.state = {
            category: CategoryData.noSection[0][1],
            background: CategoryData.noSection[0][2],
            hover: false
        }
        this.mouseOver = this.mouseOver.bind(this);
    }

    mouseOver(categoryDesc: string, categoryImage: string) {
        this.setState({
            category: categoryDesc,
            background: categoryImage,
            hover: true,
        });
    }

    renderReactElements(): Array<ReactElement> {
        let returnSection: Array<ReactElement> = [];
        let listSection: Array<ReactElement> = [];
        let key: number = 0;
        for (const item of Object.entries(CategoryData)) {
            let index: number = 0;
            item[1].forEach(category => {
                listSection.push(
                    <Link key={index++} onMouseOver={() => this.mouseOver(category[1], category[2])} to={`/${category[3]}`} className={styles.link_list}>
                        <li className={styles.points_of_list}>{this.props.t(category[0])}</li>
                    </Link>
                );
            });
            returnSection.push(
                <Fragment key={key++}>
                    <li className={styles.title_of_list}>{item[0] === 'noSection' ? '' : this.props.t(item[0])}</li>
                    <ul className={styles.link_container}>
                        {listSection}
                    </ul>
                </Fragment>
            );
            index = 0;
            listSection = [];
        }
        return returnSection;
    }
    render() {
        return(
            <div className={styles.header_container}>
                <span className={styles.header_content}>
                    <a className={styles.menu_images_container}>
                        <img className={styles.list_images} src={this.state.background} alt={this.state.category}/>
                        <div className={styles.fixed_size_container}>
                            <span className={styles.description_of_category}>{this.props.t(this.state.category)}</span>
                        </div>
                    </a>
                    <ul className={styles.list_content}>
                        {this.renderReactElements()}
                    </ul>
                </span>
            </div>
        );
    }
}

export default withTranslation(I18Namespace.Main)(Header)
