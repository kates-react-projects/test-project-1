import React, { ReactElement } from 'react';
import ProductsData from '../../json/components/products.json';
import { useTranslation } from 'react-i18next';
import { I18Namespace } from '../../i18n';
import styles from '../../less/components/products.less';
import { Link } from 'react-router-dom';

const Products: React.FC = (): ReactElement => {
    const [t] = useTranslation(I18Namespace.Main);

    function returnReactElement(): Array<ReactElement> {
        let returnMarkup: Array<ReactElement> = [];
        let key: number = 0;
        for (const item of Object.values(ProductsData)) {
            if (item && Object.keys(item).length === 4) {
                returnMarkup.push(
                    <li key={key++} className={styles.product_list_item}>
                        <a className={styles.product_link}>
                            <img className={styles.product_image}
                                src={`./images/components/products/${item.image_info[0]}`}
                                alt={`${item.image_info[1]}`}
                            />
                            <h3 className={styles.product_title}>{t(item.title)}</h3>
                            <span className={styles.product_year}>{t(item.production_year)}</span>
                            <span className={styles.product_availability}>{t(item.availability)}</span>
                        </a>
                    </li>
                );
            }
        }
        return returnMarkup;
    }

    return (
        <div className={styles.products_container}>
            <div className={styles.archive}>
                <img className={styles.archive_image} src="./images/components/products/cereal_archive.jpg" alt="Cereal Archive" />
                <div className={styles.archive_context}>
                    <h1>{t("Components.Secondary.Home.Products.Archive.title")}</h1>
                    <p>{t("Components.Secondary.Home.Products.Archive.paragraph")}</p>
                    <Link to={'/shop'} className={styles.link_like_button}>{t("Components.Secondary.Home.Products.Archive.button")}</Link>
                </div>
            </div>

            <div className={styles.featured_products}>
                <p className={styles.section_products_title}>{t("Components.Secondary.Home.Products.Features.title")}</p>
                <ul className={styles.products_list}>
                    {returnReactElement()}
                </ul>
            </div>
        </div>
    );
}

export default Products;
