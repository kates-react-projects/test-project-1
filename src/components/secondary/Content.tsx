import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { I18Namespace } from '../../i18n';
import styles from '../../less/components/content.less';

const Content: React.FC<{contentData: object}> = ({contentData}): ReactElement => {
    const [t] = useTranslation(I18Namespace.Main);

    function renderCategories(item: any): Array<ReactElement> {
        let returnMarkup: Array<ReactElement> = [];
        if (item && item.category.length > 0) {
            item.category.forEach((category: string, i: number) => {
                returnMarkup.push(
                    <a key={'a' + i} className={styles.content_links}>
                        <li>{t(category)}</li>
                    </a>
                );
                if (item.category.length !== i + 1) {
                    returnMarkup.push(<li key={'l' + i} className={styles.reference_decoration}>|</li>);
                }
            });
        }
        return returnMarkup;
    }

    function renderReactElement(): Array<ReactElement> {
        let returnMarkup: Array<ReactElement> = [];
        let key: number = 0;
        for (const item of Object.values(contentData)) {
            if (item && Object.keys(item).length === 4) {
                let section: boolean = item.type == 'landscape';
                returnMarkup.push(
                    <li key={key++} className={section? styles.content_selection_landscape : styles.content_selection_portrait}>
                        <a className={styles.img_reference}>
                            <img className={styles.content_images}
                                src={`./images/pages/${item.image_info[0]}`}
                                alt={item.image_info[1]}/>
                        </a>
                        <ul className={styles.category_reference}>
                            {renderCategories(item)}
                        </ul>
                        <div className={styles.title_and_text_content}>
                            <a className={styles.title_link_of_content_selection}>
                                <h4>{t(item.content[0])}</h4>
                            <p>{t(item.content[1])}</p>
                            </a>
                        </div>
                    </li>
                );
            }
        }
        return returnMarkup;
    }
    return (
        <div className={styles.content_container}>
            <ul className={styles.content}>
                {renderReactElement()}
            </ul>
        </div>
    );
}

export default Content;
