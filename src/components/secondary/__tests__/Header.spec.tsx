import React from "react";
import { I18nextProvider } from 'react-i18next';
import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from "enzyme";

import Header from "../Header";
import styles from '../../less/components/header.less';
import i18n from '../../../i18nForTests';

it("Renders the <Header /> class component", () => {
    const mounted = mount(
        <Router>
            <I18nextProvider i18n={i18n}>
                <Header />
            </I18nextProvider>
        </Router>
    );
    expect(mounted.contains(
        <span className={styles.description_of_category}>Components.Primary.Header.NoSection.Shop.desc</span>
    )).toBe(true);
});
